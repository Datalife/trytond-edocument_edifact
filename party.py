# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    edi_add_header = fields.Boolean('Add Header')
    edi_release_number = fields.Selection([
        (None, ' '),
        ('96A', '96A'),
        ('93A', '93A'),
        ('01B', '01B'),
        ], 'Release Number')
    edi_assigned_code = fields.Selection([
        (None, ' '),
        ('EAN001', 'EAN001'),
        ('EAN002', 'EAN002'),
        ('EAN003', 'EAN003'),
        ('EAN004', 'EAN004'),
        ('EAN005', 'EAN005'),
        ('EAN006', 'EAN006'),
        ('EAN007', 'EAN007'),
        ('EAN008', 'EAN008'),
        ('EAN009', 'EAN009'),
        ('EAN010', 'EAN010'),
        ], 'Assigned Code')


class PartyConfiguration(metaclass=PoolMeta):
    __name__ = 'party.configuration'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.identifier_types.selection.extend([
            ('EDI_sender', 'EDI Sender'),
            ('EDI_receiver', 'EDI Receiver'),
            ('EDI_supplier', 'EDI Supplier'),
            ('EDI_payer', 'EDI Payer'),
            ('EDI_buyer', 'EDI Buyer'),
            ('EDI_invoice', 'EDI Invoice')])
