# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Rule(metaclass=PoolMeta):
    __name__ = 'incoterm.rule'

    EDI_code = fields.Char('EDI Code')
