# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    EDI_code = fields.Char('EDI Code')


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    EDI_code = fields.Function(fields.Char('EDI Code'), 'get_edi_code',
        searcher='search_edi_code')

    def get_edi_code(self, name):
        return self.template.EDI_code

    @classmethod
    def search_edi_code(cls, name, clause):
        return [('template.' + clause[0],) + tuple(clause[1:])]


class Reference(metaclass=PoolMeta):
    __name__ = 'product.cross_reference'

    EDI_code = fields.Char('EAN Code')
