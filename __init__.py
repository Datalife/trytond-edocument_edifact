# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import edocument
from . import stock
from . import party
from . import product
from . import incoterm


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        configuration.ConfigurationPath,
        edocument.EdocumentMessage,
        edocument.EdocumentTemplate,
        edocument.EdocumentExportParams,
        edocument.EdocumentExportFile,
        edocument.EDocumentImportResult,
        party.Party,
        party.PartyConfiguration,
        stock.Configuration,
        stock.ConfigurationSequence,
        product.Template,
        product.Product,
        module='edocument_edifact', type_='model')

    Pool.register(
        incoterm.Rule,
        module='edocument_edifact', type_='model',
        depends=['incoterm'])

    Pool.register(
        product.Reference,
        module='edocument_edifact', type_='model',
        depends=['product_cross_reference'])
