# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)

        table = cls.__table_handler__(module_name)
        for field in ['edocument_sequence', 'EDI_output_path']:
            if table.column_exist(field):
                table.drop_column(field)


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'stock.configuration.sequence'

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)
        if exist:
            exist &= table_h.column_exist('edocument_sequence')

        super().__register__(module_name)

        if table_h.column_exist('edocument_sequence'):
            table_h.drop_column('edocument_sequence')
